### Guard {{{
##  ==========================================================================
ifndef _MAKE_SCRIPTS_PREFIX_MK_INCLUDED
define _MAKE_SCRIPTS_PREFIX_MK_INCLUDED :=
$(lastword $(MAKEFILE_LIST))
endef
##  ==========================================================================
##  }}} Guard
##
override PREFIX ?= share/make/scripts/
ifneq "$(PREFIX)" "$(firstword $(PREFIX))"
$(error $(PREFIX): Prefix not allowed: contains whitespace)
endif
ifeq "$(wildcard $(PREFIX)*)" ""
override define _makecmdargs
$(MAKE)$(if
$(MAKEFLAGS), -$(MAKEFLAGS))$(if
$(-*-command-variables-*-), $(-*-command-variables-*-))$(if
$1, $1)$(if
$(MAKECMDGOALS), $(MAKECMDGOALS))
endef
override define _MESSAGE :=
Scripts directory not found: $(PREFIX)

Either run (some variation of) the

  git submodule update --init [--force] [--recursive] [--remote]

command or manually replace every occurance of the '$(PREFIX)'
directory in the '$(_MAKE_SCRIPTS_PREFIX_MK_INCLUDED)' makefile with
'<prefix>' and re-run the initial

  $(_makecmdargs)

command or simply re-run as either the

  $(call _makecmdargs,PREFIX='<prefix>')

command or the

  PREFIX='<prefix>' $(_makecmdargs)

command, where '<prefix>' is a path to the directory containing files from the
'https://bitbucket.org/Alexander-Shukaev/make-scripts.git' repository
endef
$(error $(_MESSAGE))
override undefine _MESSAGE
override undefine _makecmdargs
endif
##
### Guard {{{
##  ==========================================================================
endif # _MAKE_SCRIPTS_PREFIX_MK_INCLUDED
##  ==========================================================================
##  }}} Guard
