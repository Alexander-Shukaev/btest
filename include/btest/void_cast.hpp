/// Preamble {{{
//  ==========================================================================
//        @file void_cast.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-15 Sunday 16:42:13 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_VOID_CAST_HPP_INCLUDED
# define BTEST_VOID_CAST_HPP_INCLUDED
//
# ifdef __cplusplus
#   define BTEST_VOID_CAST(E) (static_cast<void>((E)))
# else
#   define BTEST_VOID_CAST(E) ((void)(E))
# endif
//
# endif // BTEST_VOID_CAST_HPP_INCLUDED
//
# if defined(USING_BTEST)           || \
     defined(USING_BTEST_VOID_CAST)
#   define VOID_CAST BTEST_VOID_CAST
# endif
//
# if defined(USING_BTEST_AS_T)           || \
     defined(USING_BTEST_VOID_CAST_AS_T)
#   define T_VOID_CAST BTEST_VOID_CAST
# endif
//
# if defined(USING_BTEST_AS_BT)           || \
     defined(USING_BTEST_VOID_CAST_AS_BT)
#   define BT_VOID_CAST BTEST_VOID_CAST
# endif
