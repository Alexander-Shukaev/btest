/// Preamble {{{
//  ==========================================================================
//        @file tools.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-24 Tuesday 12:47:39 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_TOOLS_HPP_INCLUDED
# define BTEST_TOOLS_HPP_INCLUDED
//
# include "config"
//
// ---------------------------------------------------------------------------
# include <boost/test/tools/assertion_result.hpp>
//
// BUG:
//
// 'boost/test/tools/detail/expression_holder.hpp' does not include
// 'boost/test/tools/assertion_result.hpp'.
// ---------------------------------------------------------------------------
# include <boost/test/test_tools.hpp>
//
# define BTEST_LOG_DISABLE_BY_TYPE \
    BOOST_TEST_DONT_PRINT_LOG_VALUE
//
# define BTEST_ERROR \
    BOOST_TEST_ERROR
//
# define BTEST_FAIL \
    BOOST_TEST_FAIL
//
# define BTEST_IS_DEFINED \
    BOOST_TEST_IS_DEFINED
//
# endif // BTEST_TOOLS_HPP_INCLUDED
//
# if defined(USING_BTEST)       || \
     defined(USING_BTEST_TOOLS)
#   define LOG_DISABLE_BY_TYPE BTEST_LOG_DISABLE_BY_TYPE
#   define ERROR               BTEST_ERROR
#   define FAIL                BTEST_FAIL
#   define IS_DEFINED          BTEST_IS_DEFINED
# endif
//
# if defined(USING_BTEST_AS_T)       || \
     defined(USING_BTEST_TOOLS_AS_T)
#   define T_LOG_DISABLE_BY_TYPE BTEST_LOG_DISABLE_BY_TYPE
#   define T_ERROR               BTEST_ERROR
#   define T_FAIL                BTEST_FAIL
#   define T_IS_DEFINED          BTEST_IS_DEFINED
# endif
//
# if defined(USING_BTEST_AS_BT)       || \
     defined(USING_BTEST_TOOLS_AS_BT)
#   define BT_LOG_DISABLE_BY_TYPE BTEST_LOG_DISABLE_BY_TYPE
#   define BT_ERROR               BTEST_ERROR
#   define BT_FAIL                BTEST_FAIL
#   define BT_IS_DEFINED          BTEST_IS_DEFINED
# endif
