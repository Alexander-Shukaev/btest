/// Preamble {{{
//  ==========================================================================
//        @file test.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-22 Sunday 16:53:42 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_TEST_HPP_INCLUDED
# define BTEST_TEST_HPP_INCLUDED
//
# include "config"
//
# include <boost/test/unit_test_suite.hpp>
//
# ifndef BTEST_TEST_CASE_IDENTIFIER
#   define BTEST_TEST_CASE_IDENTIFIER(I) \
      I##test_case
# endif
//
# define BTEST_TEST_CASE(I) \
    BOOST_AUTO_TEST_CASE(BTEST_TEST_CASE_IDENTIFIER(I##_))
//
# define BTEST_TEST_CASE_BY_FIXTURE(I, F) \
    BOOST_FIXTURE_TEST_CASE(BTEST_TEST_CASE_IDENTIFIER(I##_), F)
//  --------------------------------------------------------------------------
# ifndef BTEST_TEST_SUITE_IDENTIFIER
#   define BTEST_TEST_SUITE_IDENTIFIER(I) \
      I##test_suite
# endif
//
# define BTEST_TEST_SUITE(I) \
    BOOST_AUTO_TEST_SUITE(BTEST_TEST_SUITE_IDENTIFIER(I##_))
//
# define BTEST_TEST_SUITE_END \
    BOOST_AUTO_TEST_SUITE_END()
//
# define BTEST_TEST_SUITE_BY_FIXTURE(I, F) \
    BOOST_FIXTURE_TEST_SUITE(BTEST_TEST_SUITE_IDENTIFIER(I##_), F)
//
# define BTEST_TEST_SUITE_BY_FIXTURE_END \
    BOOST_AUTO_TEST_SUITE_END()
//
# endif // BTEST_TEST_HPP_INCLUDED
//
# if defined(USING_BTEST)      || \
     defined(USING_BTEST_TEST)
#   define TEST_CASE                 BTEST_TEST_CASE
#   define TEST_SUITE                BTEST_TEST_SUITE
#   define TEST_SUITE_END            BTEST_TEST_SUITE_END
//  --------------------------------------------------------------------------
#   define TEST_CASE_BY_FIXTURE      BTEST_TEST_CASE_BY_FIXTURE
#   define TEST_SUITE_BY_FIXTURE     BTEST_TEST_SUITE_BY_FIXTURE
#   define TEST_SUITE_BY_FIXTURE_END BTEST_TEST_SUITE_BY_FIXTURE_END
# endif
//
# if defined(USING_BTEST_AS_T)      || \
     defined(USING_BTEST_TEST_AS_T)
#   define T_TEST_CASE                 BTEST_TEST_CASE
#   define T_TEST_SUITE                BTEST_TEST_SUITE
#   define T_TEST_SUITE_END            BTEST_TEST_SUITE_END
//  --------------------------------------------------------------------------
#   define T_TEST_CASE_BY_FIXTURE      BTEST_TEST_CASE_BY_FIXTURE
#   define T_TEST_SUITE_BY_FIXTURE     BTEST_TEST_SUITE_BY_FIXTURE
#   define T_TEST_SUITE_BY_FIXTURE_END BTEST_TEST_SUITE_BY_FIXTURE_END
# endif
//
# if defined(USING_BTEST_AS_BT)      || \
     defined(USING_BTEST_TEST_AS_BT)
#   define BT_TEST_CASE                 BTEST_TEST_CASE
#   define BT_TEST_SUITE                BTEST_TEST_SUITE
#   define BT_TEST_SUITE_END            BTEST_TEST_SUITE_END
//  --------------------------------------------------------------------------
#   define BT_TEST_CASE_BY_FIXTURE      BTEST_TEST_CASE_BY_FIXTURE
#   define BT_TEST_SUITE_BY_FIXTURE     BTEST_TEST_SUITE_BY_FIXTURE
#   define BT_TEST_SUITE_BY_FIXTURE_END BTEST_TEST_SUITE_BY_FIXTURE_END
# endif
