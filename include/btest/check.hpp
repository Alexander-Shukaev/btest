/// Preamble {{{
//  ==========================================================================
//        @file check.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-15 Sunday 18:33:46 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_CHECK_HPP_INCLUDED
# define BTEST_CHECK_HPP_INCLUDED
//
# include "config"
# include "diagnostic"
# include "exception"
# include "tools"
# include "void_cast"
//
# define BTEST_CHECK \
    BOOST_CHECK
//
# define BTEST_CHECK_IS \
    BOOST_CHECK
//
# define BTEST_CHECK_TRUE \
    BOOST_CHECK
//
# define BTEST_CHECK_T \
    BTEST_CHECK_TRUE
//
# define BTEST_CHECK_FALSE(P) \
    BOOST_CHECK(!P)
//
# define BTEST_CHECK_F \
    BTEST_CHECK_FALSE
//
# define BTEST_CHECK_GT \
    BOOST_CHECK_GT
//
# define BTEST_CHECK_GE \
    BOOST_CHECK_GE
//
# define BTEST_CHECK_LT \
    BOOST_CHECK_LT
//
# define BTEST_CHECK_LE \
    BOOST_CHECK_LE
//
# define BTEST_CHECK_NE \
    BOOST_CHECK_NE
//
# define BTEST_CHECK_NZ(V) \
    BOOST_CHECK_NE(V, 0)
//
# define BTEST_CHECK_EQ \
    BOOST_CHECK_EQUAL
//
# define BTEST_CHECK_EQUAL \
    BOOST_CHECK_EQUAL
//
# define BTEST_CHECK_ZERO(V) \
    BOOST_CHECK_EQUAL(V, 0)
//
# define BTEST_CHECK_Z(V) \
    BTEST_CHECK_ZERO(V)
//
# define BTEST_CHECK_CLOSE \
    BOOST_CHECK_CLOSE
//
# define BTEST_CHECK_CLOSE_FRACTION \
    BOOST_CHECK_CLOSE_FRACTION
//
# define BTEST_CHECK_SMALL \
    BOOST_CHECK_SMALL
//
# define BTEST_CHECK_EQ_COLLECTIONS \
    BOOST_CHECK_EQUAL_COLLECTIONS
//
# define BTEST_CHECK_EQUAL_COLLECTIONS \
    BOOST_CHECK_EQUAL_COLLECTIONS
//
# define BTEST_CHECK_PER_ELEMENT_EQ \
    BOOST_CHECK_EQUAL_COLLECTIONS
//
# define BTEST_CHECK_PER_ELEMENT_EQUAL \
    BOOST_CHECK_EQUAL_COLLECTIONS
//
# define BTEST_CHECK_ELEMENTWISE_EQ \
    BOOST_CHECK_EQUAL_COLLECTIONS
//
# define BTEST_CHECK_ELEMENTWISE_EQUAL \
    BOOST_CHECK_EQUAL_COLLECTIONS
//
# define BTEST_CHECK_BITWISE_EQ \
    BOOST_CHECK_BITWISE_EQUAL
//
# define BTEST_CHECK_BITWISE_EQUAL \
    BOOST_CHECK_BITWISE_EQUAL
//
# define BTEST_CHECK_PREDICATE \
    BOOST_CHECK_PREDICATE
//
# define BTEST_CHECK_MESSAGE \
    BOOST_CHECK_MESSAGE
//
# define BTEST_CHECK_NO_THROW \
    BOOST_CHECK_NO_THROW
//
# define BTEST_CHECK_THROW \
    BOOST_CHECK_THROW
//
# ifdef NDEBUG
#   ifndef BTEST_CHECK_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#     define BTEST_CHECK_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   endif
# endif
//
# ifdef BTEST_CHECK_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   define BTEST_CHECK_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_VOID_CAST(e)
# else
#   define BTEST_CHECK_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_EXCEPTION_LOG_DIAGNOSTIC(e)
# endif
//
# define BTEST_CHECK_EXCEPTION_PREDICATE_IDENTIFIER \
    BTEST_EXCEPTION_PREDICATE_IDENTIFIER_BY_LEVEL(CHECK)
//
# define BTEST_CHECK_EXCEPTION_PREDICATE(E) \
    BTEST_EXCEPTION_PREDICATE_BY_LEVEL(CHECK, E)
//
# define BTEST_CHECK_EXCEPTION_BY_PREDICATE(S, E, P) \
    BTEST_EXCEPTION_BY_PREDICATE_BY_LEVEL(CHECK, S, E, P)
//
# define BTEST_CHECK_EXCEPTION(S, E, I) \
    BTEST_EXCEPTION_BY_LEVEL(CHECK, S, E, I)
//
# define BTEST_CHECK_EX_PREDICATE \
    BTEST_CHECK_EXCEPTION_PREDICATE
//
# define BTEST_CHECK_EX_BY_PREDICATE \
    BTEST_CHECK_EXCEPTION_BY_PREDICATE
//
# define BTEST_CHECK_EX \
    BTEST_CHECK_EXCEPTION
//
# endif // BTEST_CHECK_HPP_INCLUDED
//
# if defined(USING_BTEST) || defined(USING_BTEST_CHECK)
#   define CHECK \
      BTEST_CHECK
#   define CHECK_IS \
      BTEST_CHECK_IS
#   define CHECK_TRUE \
      BTEST_CHECK_TRUE
#   define CHECK_T \
      BTEST_CHECK_T
#   define CHECK_FALSE \
      BTEST_CHECK_FALSE
#   define CHECK_F \
      BTEST_CHECK_F
#   define CHECK_GT \
      BTEST_CHECK_GT
#   define CHECK_GE \
      BTEST_CHECK_GE
#   define CHECK_LT \
      BTEST_CHECK_LT
#   define CHECK_LE \
      BTEST_CHECK_LE
#   define CHECK_NE \
      BTEST_CHECK_NE
#   define CHECK_NZ \
      BTEST_CHECK_NZ
#   define CHECK_EQ \
      BTEST_CHECK_EQ
#   define CHECK_EQUAL \
      BTEST_CHECK_EQUAL
#   define CHECK_ZERO \
      BTEST_CHECK_ZERO
#   define CHECK_Z \
      BTEST_CHECK_Z
#   define CHECK_CLOSE \
      BTEST_CHECK_CLOSE
#   define CHECK_CLOSE_FRACTION \
      BTEST_CHECK_CLOSE_FRACTION
#   define CHECK_SMALL \
      BTEST_CHECK_SMALL
#   define CHECK_EQ_COLLECTIONS \
      BTEST_CHECK_EQ_COLLECTIONS
#   define CHECK_EQUAL_COLLECTIONS \
      BTEST_CHECK_EQUAL_COLLECTIONS
#   define CHECK_PER_ELEMENT_EQ \
      BTEST_CHECK_PER_ELEMENT_EQ
#   define CHECK_PER_ELEMENT_EQUAL \
      BTEST_CHECK_PER_ELEMENT_EQUAL
#   define CHECK_ELEMENTWISE_EQ \
      BTEST_CHECK_ELEMENTWISE_EQ
#   define CHECK_ELEMENTWISE_EQUAL \
      BTEST_CHECK_ELEMENTWISE_EQUAL
#   define CHECK_BITWISE_EQ \
      BTEST_CHECK_BITWISE_EQ
#   define CHECK_BITWISE_EQUAL \
      BTEST_CHECK_BITWISE_EQUAL
#   define CHECK_PREDICATE \
      BTEST_CHECK_PREDICATE
#   define CHECK_MESSAGE \
      BTEST_CHECK_MESSAGE
#   define CHECK_NO_THROW \
      BTEST_CHECK_NO_THROW
#   define CHECK_THROW \
      BTEST_CHECK_THROW
#   define CHECK_EXCEPTION_PREDICATE \
      BTEST_CHECK_EXCEPTION_PREDICATE
#   define CHECK_EXCEPTION_BY_PREDICATE \
      BTEST_CHECK_EXCEPTION_BY_PREDICATE
#   define CHECK_EXCEPTION \
      BTEST_CHECK_EXCEPTION
#   define CHECK_EX_PREDICATE \
      BTEST_CHECK_EX_PREDICATE
#   define CHECK_EX_BY_PREDICATE \
      BTEST_CHECK_EX_BY_PREDICATE
#   define CHECK_EX \
      BTEST_CHECK_EX
# endif
//
# if defined(USING_BTEST_AS_T) || defined(USING_BTEST_CHECK_AS_T)
#   define T_CHECK \
      BTEST_CHECK
#   define T_CHECK_IS \
      BTEST_CHECK_IS
#   define T_CHECK_TRUE \
      BTEST_CHECK_TRUE
#   define T_CHECK_T \
      BTEST_CHECK_T
#   define T_CHECK_FALSE \
      BTEST_CHECK_FALSE
#   define T_CHECK_F \
      BTEST_CHECK_F
#   define T_CHECK_GT \
      BTEST_CHECK_GT
#   define T_CHECK_GE \
      BTEST_CHECK_GE
#   define T_CHECK_LT \
      BTEST_CHECK_LT
#   define T_CHECK_LE \
      BTEST_CHECK_LE
#   define T_CHECK_NE \
      BTEST_CHECK_NE
#   define T_CHECK_NZ \
      BTEST_CHECK_NZ
#   define T_CHECK_EQ \
      BTEST_CHECK_EQ
#   define T_CHECK_EQUAL \
      BTEST_CHECK_EQUAL
#   define T_CHECK_ZERO \
      BTEST_CHECK_ZERO
#   define T_CHECK_Z \
      BTEST_CHECK_Z
#   define T_CHECK_CLOSE \
      BTEST_CHECK_CLOSE
#   define T_CHECK_CLOSE_FRACTION \
      BTEST_CHECK_CLOSE_FRACTION
#   define T_CHECK_SMALL \
      BTEST_CHECK_SMALL
#   define T_CHECK_EQ_COLLECTIONS \
      BTEST_CHECK_EQ_COLLECTIONS
#   define T_CHECK_EQUAL_COLLECTIONS \
      BTEST_CHECK_EQUAL_COLLECTIONS
#   define T_CHECK_PER_ELEMENT_EQ \
      BTEST_CHECK_PER_ELEMENT_EQ
#   define T_CHECK_PER_ELEMENT_EQUAL \
      BTEST_CHECK_PER_ELEMENT_EQUAL
#   define T_CHECK_ELEMENTWISE_EQ \
      BTEST_CHECK_ELEMENTWISE_EQ
#   define T_CHECK_ELEMENTWISE_EQUAL \
      BTEST_CHECK_ELEMENTWISE_EQUAL
#   define T_CHECK_BITWISE_EQ \
      BTEST_CHECK_BITWISE_EQ
#   define T_CHECK_BITWISE_EQUAL \
      BTEST_CHECK_BITWISE_EQUAL
#   define T_CHECK_PREDICATE \
      BTEST_CHECK_PREDICATE
#   define T_CHECK_MESSAGE \
      BTEST_CHECK_MESSAGE
#   define T_CHECK_NO_THROW \
      BTEST_CHECK_NO_THROW
#   define T_CHECK_THROW \
      BTEST_CHECK_THROW
#   define T_CHECK_EXCEPTION_PREDICATE \
      BTEST_CHECK_EXCEPTION_PREDICATE
#   define T_CHECK_EXCEPTION_BY_PREDICATE \
      BTEST_CHECK_EXCEPTION_BY_PREDICATE
#   define T_CHECK_EXCEPTION \
      BTEST_CHECK_EXCEPTION
#   define T_CHECK_EX_PREDICATE \
      BTEST_CHECK_EX_PREDICATE
#   define T_CHECK_EX_BY_PREDICATE \
      BTEST_CHECK_EX_BY_PREDICATE
#   define T_CHECK_EX \
      BTEST_CHECK_EX
# endif
//
# if defined(USING_BTEST_AS_BT) || defined(USING_BTEST_CHECK_AS_BT)
#   define BT_CHECK \
      BTEST_CHECK
#   define BT_CHECK_IS \
      BTEST_CHECK_IS
#   define BT_CHECK_TRUE \
      BTEST_CHECK_TRUE
#   define BT_CHECK_T \
      BTEST_CHECK_T
#   define BT_CHECK_FALSE \
      BTEST_CHECK_FALSE
#   define BT_CHECK_F \
      BTEST_CHECK_F
#   define BT_CHECK_GT \
      BTEST_CHECK_GT
#   define BT_CHECK_GE \
      BTEST_CHECK_GE
#   define BT_CHECK_LT \
      BTEST_CHECK_LT
#   define BT_CHECK_LE \
      BTEST_CHECK_LE
#   define BT_CHECK_NE \
      BTEST_CHECK_NE
#   define BT_CHECK_NZ \
      BTEST_CHECK_NZ
#   define BT_CHECK_EQ \
      BTEST_CHECK_EQ
#   define BT_CHECK_EQUAL \
      BTEST_CHECK_EQUAL
#   define BT_CHECK_ZERO \
      BTEST_CHECK_ZERO
#   define BT_CHECK_Z \
      BTEST_CHECK_Z
#   define BT_CHECK_CLOSE \
      BTEST_CHECK_CLOSE
#   define BT_CHECK_CLOSE_FRACTION \
      BTEST_CHECK_CLOSE_FRACTION
#   define BT_CHECK_SMALL \
      BTEST_CHECK_SMALL
#   define BT_CHECK_EQ_COLLECTIONS \
      BTEST_CHECK_EQ_COLLECTIONS
#   define BT_CHECK_EQUAL_COLLECTIONS \
      BTEST_CHECK_EQUAL_COLLECTIONS
#   define BT_CHECK_PER_ELEMENT_EQ \
      BTEST_CHECK_PER_ELEMENT_EQ
#   define BT_CHECK_PER_ELEMENT_EQUAL \
      BTEST_CHECK_PER_ELEMENT_EQUAL
#   define BT_CHECK_ELEMENTWISE_EQ \
      BTEST_CHECK_ELEMENTWISE_EQ
#   define BT_CHECK_ELEMENTWISE_EQUAL \
      BTEST_CHECK_ELEMENTWISE_EQUAL
#   define BT_CHECK_BITWISE_EQ \
      BTEST_CHECK_BITWISE_EQ
#   define BT_CHECK_BITWISE_EQUAL \
      BTEST_CHECK_BITWISE_EQUAL
#   define BT_CHECK_PREDICATE \
      BTEST_CHECK_PREDICATE
#   define BT_CHECK_MESSAGE \
      BTEST_CHECK_MESSAGE
#   define BT_CHECK_NO_THROW \
      BTEST_CHECK_NO_THROW
#   define BT_CHECK_THROW \
      BTEST_CHECK_THROW
#   define BT_CHECK_EXCEPTION_PREDICATE \
      BTEST_CHECK_EXCEPTION_PREDICATE
#   define BT_CHECK_EXCEPTION_BY_PREDICATE \
      BTEST_CHECK_EXCEPTION_BY_PREDICATE
#   define BT_CHECK_EXCEPTION \
      BTEST_CHECK_EXCEPTION
#   define BT_CHECK_EX_PREDICATE \
      BTEST_CHECK_EX_PREDICATE
#   define BT_CHECK_EX_BY_PREDICATE \
      BTEST_CHECK_EX_BY_PREDICATE
#   define BT_CHECK_EX \
      BTEST_CHECK_EX
# endif
