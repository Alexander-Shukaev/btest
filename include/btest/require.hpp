/// Preamble {{{
//  ==========================================================================
//        @file require.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-15 Sunday 18:33:46 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_REQUIRE_HPP_INCLUDED
# define BTEST_REQUIRE_HPP_INCLUDED
//
# include "config"
# include "diagnostic"
# include "exception"
# include "tools"
# include "void_cast"
//
# define BTEST_REQUIRE \
    BOOST_REQUIRE
//
# define BTEST_REQUIRE_IS \
    BOOST_REQUIRE
//
# define BTEST_REQUIRE_TRUE \
    BOOST_REQUIRE
//
# define BTEST_REQUIRE_T \
    BTEST_REQUIRE_TRUE
//
# define BTEST_REQUIRE_FALSE(P) \
    BOOST_REQUIRE(!P)
//
# define BTEST_REQUIRE_F \
    BTEST_REQUIRE_FALSE
//
# define BTEST_REQUIRE_GT \
    BOOST_REQUIRE_GT
//
# define BTEST_REQUIRE_GE \
    BOOST_REQUIRE_GE
//
# define BTEST_REQUIRE_LT \
    BOOST_REQUIRE_LT
//
# define BTEST_REQUIRE_LE \
    BOOST_REQUIRE_LE
//
# define BTEST_REQUIRE_NE \
    BOOST_REQUIRE_NE
//
# define BTEST_REQUIRE_NZ(V) \
    BOOST_REQUIRE_NE(V, 0)
//
# define BTEST_REQUIRE_EQ \
    BOOST_REQUIRE_EQUAL
//
# define BTEST_REQUIRE_EQUAL \
    BOOST_REQUIRE_EQUAL
//
# define BTEST_REQUIRE_ZERO(V) \
    BOOST_REQUIRE_EQUAL(V, 0)
//
# define BTEST_REQUIRE_Z(V) \
    BTEST_REQUIRE_ZERO(V)
//
# define BTEST_REQUIRE_CLOSE \
    BOOST_REQUIRE_CLOSE
//
# define BTEST_REQUIRE_CLOSE_FRACTION \
    BOOST_REQUIRE_CLOSE_FRACTION
//
# define BTEST_REQUIRE_SMALL \
    BOOST_REQUIRE_SMALL
//
# define BTEST_REQUIRE_EQ_COLLECTIONS \
    BOOST_REQUIRE_EQUAL_COLLECTIONS
//
# define BTEST_REQUIRE_EQUAL_COLLECTIONS \
    BOOST_REQUIRE_EQUAL_COLLECTIONS
//
# define BTEST_REQUIRE_PER_ELEMENT_EQ \
    BOOST_REQUIRE_EQUAL_COLLECTIONS
//
# define BTEST_REQUIRE_PER_ELEMENT_EQUAL \
    BOOST_REQUIRE_EQUAL_COLLECTIONS
//
# define BTEST_REQUIRE_ELEMENTWISE_EQ \
    BOOST_REQUIRE_EQUAL_COLLECTIONS
//
# define BTEST_REQUIRE_ELEMENTWISE_EQUAL \
    BOOST_REQUIRE_EQUAL_COLLECTIONS
//
# define BTEST_REQUIRE_BITWISE_EQ \
    BOOST_REQUIRE_BITWISE_EQUAL
//
# define BTEST_REQUIRE_BITWISE_EQUAL \
    BOOST_REQUIRE_BITWISE_EQUAL
//
# define BTEST_REQUIRE_PREDICATE \
    BOOST_REQUIRE_PREDICATE
//
# define BTEST_REQUIRE_MESSAGE \
    BOOST_REQUIRE_MESSAGE
//
# define BTEST_REQUIRE_NO_THROW \
    BOOST_REQUIRE_NO_THROW
//
# define BTEST_REQUIRE_THROW \
    BOOST_REQUIRE_THROW
//
# ifdef NDEBUG
#   ifndef BTEST_REQUIRE_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#     define BTEST_REQUIRE_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   endif
# endif
//
# ifdef BTEST_REQUIRE_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   define BTEST_REQUIRE_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_VOID_CAST(e)
# else
#   define BTEST_REQUIRE_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_EXCEPTION_LOG_DIAGNOSTIC(e)
# endif
//
# define BTEST_REQUIRE_EXCEPTION_PREDICATE_IDENTIFIER \
    BTEST_EXCEPTION_PREDICATE_IDENTIFIER_BY_LEVEL(REQUIRE)
//
# define BTEST_REQUIRE_EXCEPTION_PREDICATE(E) \
    BTEST_EXCEPTION_PREDICATE_BY_LEVEL(REQUIRE, E)
//
# define BTEST_REQUIRE_EXCEPTION_BY_PREDICATE(S, E, P) \
    BTEST_EXCEPTION_BY_PREDICATE_BY_LEVEL(REQUIRE, S, E, P)
//
# define BTEST_REQUIRE_EXCEPTION(S, E, I) \
    BTEST_EXCEPTION_BY_LEVEL(REQUIRE, S, E, I)
//
# define BTEST_REQUIRE_EX_PREDICATE \
    BTEST_REQUIRE_EXCEPTION_PREDICATE
//
# define BTEST_REQUIRE_EX_BY_PREDICATE \
    BTEST_REQUIRE_EXCEPTION_BY_PREDICATE
//
# define BTEST_REQUIRE_EX \
    BTEST_REQUIRE_EXCEPTION
//
# endif // BTEST_REQUIRE_HPP_INCLUDED
//
# if defined(USING_BTEST) || defined(USING_BTEST_REQUIRE)
#   define REQUIRE \
      BTEST_REQUIRE
#   define REQUIRE_IS \
      BTEST_REQUIRE_IS
#   define REQUIRE_TRUE \
      BTEST_REQUIRE_TRUE
#   define REQUIRE_T \
      BTEST_REQUIRE_T
#   define REQUIRE_FALSE \
      BTEST_REQUIRE_FALSE
#   define REQUIRE_F \
      BTEST_REQUIRE_F
#   define REQUIRE_GT \
      BTEST_REQUIRE_GT
#   define REQUIRE_GE \
      BTEST_REQUIRE_GE
#   define REQUIRE_LT \
      BTEST_REQUIRE_LT
#   define REQUIRE_LE \
      BTEST_REQUIRE_LE
#   define REQUIRE_NE \
      BTEST_REQUIRE_NE
#   define REQUIRE_NZ \
      BTEST_REQUIRE_NZ
#   define REQUIRE_EQ \
      BTEST_REQUIRE_EQ
#   define REQUIRE_EQUAL \
      BTEST_REQUIRE_EQUAL
#   define REQUIRE_ZERO \
      BTEST_REQUIRE_ZERO
#   define REQUIRE_Z \
      BTEST_REQUIRE_Z
#   define REQUIRE_CLOSE \
      BTEST_REQUIRE_CLOSE
#   define REQUIRE_CLOSE_FRACTION \
      BTEST_REQUIRE_CLOSE_FRACTION
#   define REQUIRE_SMALL \
      BTEST_REQUIRE_SMALL
#   define REQUIRE_EQ_COLLECTIONS \
      BTEST_REQUIRE_EQ_COLLECTIONS
#   define REQUIRE_EQUAL_COLLECTIONS \
      BTEST_REQUIRE_EQUAL_COLLECTIONS
#   define REQUIRE_PER_ELEMENT_EQ \
      BTEST_REQUIRE_PER_ELEMENT_EQ
#   define REQUIRE_PER_ELEMENT_EQUAL \
      BTEST_REQUIRE_PER_ELEMENT_EQUAL
#   define REQUIRE_ELEMENTWISE_EQ \
      BTEST_REQUIRE_ELEMENTWISE_EQ
#   define REQUIRE_ELEMENTWISE_EQUAL \
      BTEST_REQUIRE_ELEMENTWISE_EQUAL
#   define REQUIRE_BITWISE_EQ \
      BTEST_REQUIRE_BITWISE_EQ
#   define REQUIRE_BITWISE_EQUAL \
      BTEST_REQUIRE_BITWISE_EQUAL
#   define REQUIRE_PREDICATE \
      BTEST_REQUIRE_PREDICATE
#   define REQUIRE_MESSAGE \
      BTEST_REQUIRE_MESSAGE
#   define REQUIRE_NO_THROW \
      BTEST_REQUIRE_NO_THROW
#   define REQUIRE_THROW \
      BTEST_REQUIRE_THROW
#   define REQUIRE_EXCEPTION_PREDICATE \
      BTEST_REQUIRE_EXCEPTION_PREDICATE
#   define REQUIRE_EXCEPTION_BY_PREDICATE \
      BTEST_REQUIRE_EXCEPTION_BY_PREDICATE
#   define REQUIRE_EXCEPTION \
      BTEST_REQUIRE_EXCEPTION
#   define REQUIRE_EX_PREDICATE \
      BTEST_REQUIRE_EX_PREDICATE
#   define REQUIRE_EX_BY_PREDICATE \
      BTEST_REQUIRE_EX_BY_PREDICATE
#   define REQUIRE_EX \
      BTEST_REQUIRE_EX
# endif
//
# if defined(USING_BTEST_AS_T) || defined(USING_BTEST_REQUIRE_AS_T)
#   define T_REQUIRE \
      BTEST_REQUIRE
#   define T_REQUIRE_IS \
      BTEST_REQUIRE_IS
#   define T_REQUIRE_TRUE \
      BTEST_REQUIRE_TRUE
#   define T_REQUIRE_T \
      BTEST_REQUIRE_T
#   define T_REQUIRE_FALSE \
      BTEST_REQUIRE_FALSE
#   define T_REQUIRE_F \
      BTEST_REQUIRE_F
#   define T_REQUIRE_GT \
      BTEST_REQUIRE_GT
#   define T_REQUIRE_GE \
      BTEST_REQUIRE_GE
#   define T_REQUIRE_LT \
      BTEST_REQUIRE_LT
#   define T_REQUIRE_LE \
      BTEST_REQUIRE_LE
#   define T_REQUIRE_NE \
      BTEST_REQUIRE_NE
#   define T_REQUIRE_NZ \
      BTEST_REQUIRE_NZ
#   define T_REQUIRE_EQ \
      BTEST_REQUIRE_EQ
#   define T_REQUIRE_EQUAL \
      BTEST_REQUIRE_EQUAL
#   define T_REQUIRE_ZERO \
      BTEST_REQUIRE_ZERO
#   define T_REQUIRE_Z \
      BTEST_REQUIRE_Z
#   define T_REQUIRE_CLOSE \
      BTEST_REQUIRE_CLOSE
#   define T_REQUIRE_CLOSE_FRACTION \
      BTEST_REQUIRE_CLOSE_FRACTION
#   define T_REQUIRE_SMALL \
      BTEST_REQUIRE_SMALL
#   define T_REQUIRE_EQ_COLLECTIONS \
      BTEST_REQUIRE_EQ_COLLECTIONS
#   define T_REQUIRE_EQUAL_COLLECTIONS \
      BTEST_REQUIRE_EQUAL_COLLECTIONS
#   define T_REQUIRE_PER_ELEMENT_EQ \
      BTEST_REQUIRE_PER_ELEMENT_EQ
#   define T_REQUIRE_PER_ELEMENT_EQUAL \
      BTEST_REQUIRE_PER_ELEMENT_EQUAL
#   define T_REQUIRE_ELEMENTWISE_EQ \
      BTEST_REQUIRE_ELEMENTWISE_EQ
#   define T_REQUIRE_ELEMENTWISE_EQUAL \
      BTEST_REQUIRE_ELEMENTWISE_EQUAL
#   define T_REQUIRE_BITWISE_EQ \
      BTEST_REQUIRE_BITWISE_EQ
#   define T_REQUIRE_BITWISE_EQUAL \
      BTEST_REQUIRE_BITWISE_EQUAL
#   define T_REQUIRE_PREDICATE \
      BTEST_REQUIRE_PREDICATE
#   define T_REQUIRE_MESSAGE \
      BTEST_REQUIRE_MESSAGE
#   define T_REQUIRE_NO_THROW \
      BTEST_REQUIRE_NO_THROW
#   define T_REQUIRE_THROW \
      BTEST_REQUIRE_THROW
#   define T_REQUIRE_EXCEPTION_PREDICATE \
      BTEST_REQUIRE_EXCEPTION_PREDICATE
#   define T_REQUIRE_EXCEPTION_BY_PREDICATE \
      BTEST_REQUIRE_EXCEPTION_BY_PREDICATE
#   define T_REQUIRE_EXCEPTION \
      BTEST_REQUIRE_EXCEPTION
#   define T_REQUIRE_EX_PREDICATE \
      BTEST_REQUIRE_EX_PREDICATE
#   define T_REQUIRE_EX_BY_PREDICATE \
      BTEST_REQUIRE_EX_BY_PREDICATE
#   define T_REQUIRE_EX \
      BTEST_REQUIRE_EX
# endif
//
# if defined(USING_BTEST_AS_BT) || defined(USING_BTEST_REQUIRE_AS_BT)
#   define BT_REQUIRE \
      BTEST_REQUIRE
#   define BT_REQUIRE_IS \
      BTEST_REQUIRE_IS
#   define BT_REQUIRE_TRUE \
      BTEST_REQUIRE_TRUE
#   define BT_REQUIRE_T \
      BTEST_REQUIRE_T
#   define BT_REQUIRE_FALSE \
      BTEST_REQUIRE_FALSE
#   define BT_REQUIRE_F \
      BTEST_REQUIRE_F
#   define BT_REQUIRE_GT \
      BTEST_REQUIRE_GT
#   define BT_REQUIRE_GE \
      BTEST_REQUIRE_GE
#   define BT_REQUIRE_LT \
      BTEST_REQUIRE_LT
#   define BT_REQUIRE_LE \
      BTEST_REQUIRE_LE
#   define BT_REQUIRE_NE \
      BTEST_REQUIRE_NE
#   define BT_REQUIRE_NZ \
      BTEST_REQUIRE_NZ
#   define BT_REQUIRE_EQ \
      BTEST_REQUIRE_EQ
#   define BT_REQUIRE_EQUAL \
      BTEST_REQUIRE_EQUAL
#   define BT_REQUIRE_ZERO \
      BTEST_REQUIRE_ZERO
#   define BT_REQUIRE_Z \
      BTEST_REQUIRE_Z
#   define BT_REQUIRE_CLOSE \
      BTEST_REQUIRE_CLOSE
#   define BT_REQUIRE_CLOSE_FRACTION \
      BTEST_REQUIRE_CLOSE_FRACTION
#   define BT_REQUIRE_SMALL \
      BTEST_REQUIRE_SMALL
#   define BT_REQUIRE_EQ_COLLECTIONS \
      BTEST_REQUIRE_EQ_COLLECTIONS
#   define BT_REQUIRE_EQUAL_COLLECTIONS \
      BTEST_REQUIRE_EQUAL_COLLECTIONS
#   define BT_REQUIRE_PER_ELEMENT_EQ \
      BTEST_REQUIRE_PER_ELEMENT_EQ
#   define BT_REQUIRE_PER_ELEMENT_EQUAL \
      BTEST_REQUIRE_PER_ELEMENT_EQUAL
#   define BT_REQUIRE_ELEMENTWISE_EQ \
      BTEST_REQUIRE_ELEMENTWISE_EQ
#   define BT_REQUIRE_ELEMENTWISE_EQUAL \
      BTEST_REQUIRE_ELEMENTWISE_EQUAL
#   define BT_REQUIRE_BITWISE_EQ \
      BTEST_REQUIRE_BITWISE_EQ
#   define BT_REQUIRE_BITWISE_EQUAL \
      BTEST_REQUIRE_BITWISE_EQUAL
#   define BT_REQUIRE_PREDICATE \
      BTEST_REQUIRE_PREDICATE
#   define BT_REQUIRE_MESSAGE \
      BTEST_REQUIRE_MESSAGE
#   define BT_REQUIRE_NO_THROW \
      BTEST_REQUIRE_NO_THROW
#   define BT_REQUIRE_THROW \
      BTEST_REQUIRE_THROW
#   define BT_REQUIRE_EXCEPTION_PREDICATE \
      BTEST_REQUIRE_EXCEPTION_PREDICATE
#   define BT_REQUIRE_EXCEPTION_BY_PREDICATE \
      BTEST_REQUIRE_EXCEPTION_BY_PREDICATE
#   define BT_REQUIRE_EXCEPTION \
      BTEST_REQUIRE_EXCEPTION
#   define BT_REQUIRE_EX_PREDICATE \
      BTEST_REQUIRE_EX_PREDICATE
#   define BT_REQUIRE_EX_BY_PREDICATE \
      BTEST_REQUIRE_EX_BY_PREDICATE
#   define BT_REQUIRE_EX \
      BTEST_REQUIRE_EX
# endif
