/// Preamble {{{
//  ==========================================================================
//        @file log.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-15 Sunday 16:34:15 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_LOG_HPP_INCLUDED
# define BTEST_LOG_HPP_INCLUDED
//
# include "config"
# include "diagnostic"
//
# include <boost/test/unit_test_log.hpp>
//
# define BTEST_LOG_DIAGNOSTIC(E) \
    BOOST_TEST_MESSAGE(BTEST_DIAGNOSTIC(E))
//
# define BTEST_LOG_MESSAGE(M) \
    BOOST_TEST_MESSAGE(M)
//
# define BTEST_LOG_CHECKPOINT(M) \
    BOOST_TEST_CHECKPOINT(M)
//
# define BTEST_LOG_PASSPOINT() \
    BOOST_TEST_PASSPOINT()
//
# endif // BTEST_LOG_HPP_INCLUDED
//
# if defined(USING_BTEST)     || \
     defined(USING_BTEST_LOG)
#   define LOG_DIAGNOSTIC BTEST_LOG_DIAGNOSTIC
#   define LOG_MESSAGE    BTEST_LOG_MESSAGE
#   define LOG_CHECKPOINT BTEST_LOG_CHECKPOINT
#   define LOG_PASSPOINT  BTEST_LOG_PASSPOINT
# endif
//
# if defined(USING_BTEST_AS_T)     || \
     defined(USING_BTEST_LOG_AS_T)
#   define T_LOG_DIAGNOSTIC BTEST_LOG_DIAGNOSTIC
#   define T_LOG_MESSAGE    BTEST_LOG_MESSAGE
#   define T_LOG_CHECKPOINT BTEST_LOG_CHECKPOINT
#   define T_LOG_PASSPOINT  BTEST_LOG_PASSPOINT
# endif
//
# if defined(USING_BTEST_AS_BT)     || \
     defined(USING_BTEST_LOG_AS_BT)
#   define BT_LOG_DIAGNOSTIC BTEST_LOG_DIAGNOSTIC
#   define BT_LOG_MESSAGE    BTEST_LOG_MESSAGE
#   define BT_LOG_CHECKPOINT BTEST_LOG_CHECKPOINT
#   define BT_LOG_PASSPOINT  BTEST_LOG_PASSPOINT
# endif
