/// Preamble {{{
//  ==========================================================================
//        @file warn.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-15 Sunday 18:33:46 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_WARN_HPP_INCLUDED
# define BTEST_WARN_HPP_INCLUDED
//
# include "config"
# include "diagnostic"
# include "exception"
# include "tools"
# include "void_cast"
//
# define BTEST_WARN \
    BOOST_WARN
//
# define BTEST_WARN_IS \
    BOOST_WARN
//
# define BTEST_WARN_TRUE \
    BOOST_WARN
//
# define BTEST_WARN_T \
    BTEST_WARN_TRUE
//
# define BTEST_WARN_FALSE(P) \
    BOOST_WARN(!P)
//
# define BTEST_WARN_F \
    BTEST_WARN_FALSE
//
# define BTEST_WARN_GT \
    BOOST_WARN_GT
//
# define BTEST_WARN_GE \
    BOOST_WARN_GE
//
# define BTEST_WARN_LT \
    BOOST_WARN_LT
//
# define BTEST_WARN_LE \
    BOOST_WARN_LE
//
# define BTEST_WARN_NE \
    BOOST_WARN_NE
//
# define BTEST_WARN_NZ(V) \
    BOOST_WARN_NE(V, 0)
//
# define BTEST_WARN_EQ \
    BOOST_WARN_EQUAL
//
# define BTEST_WARN_EQUAL \
    BOOST_WARN_EQUAL
//
# define BTEST_WARN_ZERO(V) \
    BOOST_WARN_EQUAL(V, 0)
//
# define BTEST_WARN_Z(V) \
    BTEST_WARN_ZERO(V)
//
# define BTEST_WARN_CLOSE \
    BOOST_WARN_CLOSE
//
# define BTEST_WARN_CLOSE_FRACTION \
    BOOST_WARN_CLOSE_FRACTION
//
# define BTEST_WARN_SMALL \
    BOOST_WARN_SMALL
//
# define BTEST_WARN_EQ_COLLECTIONS \
    BOOST_WARN_EQUAL_COLLECTIONS
//
# define BTEST_WARN_EQUAL_COLLECTIONS \
    BOOST_WARN_EQUAL_COLLECTIONS
//
# define BTEST_WARN_PER_ELEMENT_EQ \
    BOOST_WARN_EQUAL_COLLECTIONS
//
# define BTEST_WARN_PER_ELEMENT_EQUAL \
    BOOST_WARN_EQUAL_COLLECTIONS
//
# define BTEST_WARN_ELEMENTWISE_EQ \
    BOOST_WARN_EQUAL_COLLECTIONS
//
# define BTEST_WARN_ELEMENTWISE_EQUAL \
    BOOST_WARN_EQUAL_COLLECTIONS
//
# define BTEST_WARN_BITWISE_EQ \
    BOOST_WARN_BITWISE_EQUAL
//
# define BTEST_WARN_BITWISE_EQUAL \
    BOOST_WARN_BITWISE_EQUAL
//
# define BTEST_WARN_PREDICATE \
    BOOST_WARN_PREDICATE
//
# define BTEST_WARN_MESSAGE \
    BOOST_WARN_MESSAGE
//
# define BTEST_WARN_NO_THROW \
    BOOST_WARN_NO_THROW
//
# define BTEST_WARN_THROW \
    BOOST_WARN_THROW
//
# ifdef NDEBUG
#   ifndef BTEST_WARN_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#     define BTEST_WARN_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   endif
# endif
//
# ifdef BTEST_WARN_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   define BTEST_WARN_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_VOID_CAST(e)
# else
#   define BTEST_WARN_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_EXCEPTION_LOG_DIAGNOSTIC(e)
# endif
//
# define BTEST_WARN_EXCEPTION_PREDICATE_IDENTIFIER \
    BTEST_EXCEPTION_PREDICATE_IDENTIFIER_BY_LEVEL(WARN)
//
# define BTEST_WARN_EXCEPTION_PREDICATE(E) \
    BTEST_EXCEPTION_PREDICATE_BY_LEVEL(WARN, E)
//
# define BTEST_WARN_EXCEPTION_BY_PREDICATE(S, E, P) \
    BTEST_EXCEPTION_BY_PREDICATE_BY_LEVEL(WARN, S, E, P)
//
# define BTEST_WARN_EXCEPTION(S, E, I) \
    BTEST_EXCEPTION_BY_LEVEL(WARN, S, E, I)
//
# define BTEST_WARN_EX_PREDICATE \
    BTEST_WARN_EXCEPTION_PREDICATE
//
# define BTEST_WARN_EX_BY_PREDICATE \
    BTEST_WARN_EXCEPTION_BY_PREDICATE
//
# define BTEST_WARN_EX \
    BTEST_WARN_EXCEPTION
//
# endif // BTEST_WARN_HPP_INCLUDED
//
# if defined(USING_BTEST) || defined(USING_BTEST_WARN)
#   define WARN \
      BTEST_WARN
#   define WARN_IS \
      BTEST_WARN_IS
#   define WARN_TRUE \
      BTEST_WARN_TRUE
#   define WARN_T \
      BTEST_WARN_T
#   define WARN_FALSE \
      BTEST_WARN_FALSE
#   define WARN_F \
      BTEST_WARN_F
#   define WARN_GT \
      BTEST_WARN_GT
#   define WARN_GE \
      BTEST_WARN_GE
#   define WARN_LT \
      BTEST_WARN_LT
#   define WARN_LE \
      BTEST_WARN_LE
#   define WARN_NE \
      BTEST_WARN_NE
#   define WARN_NZ \
      BTEST_WARN_NZ
#   define WARN_EQ \
      BTEST_WARN_EQ
#   define WARN_EQUAL \
      BTEST_WARN_EQUAL
#   define WARN_ZERO \
      BTEST_WARN_ZERO
#   define WARN_Z \
      BTEST_WARN_Z
#   define WARN_CLOSE \
      BTEST_WARN_CLOSE
#   define WARN_CLOSE_FRACTION \
      BTEST_WARN_CLOSE_FRACTION
#   define WARN_SMALL \
      BTEST_WARN_SMALL
#   define WARN_EQ_COLLECTIONS \
      BTEST_WARN_EQ_COLLECTIONS
#   define WARN_EQUAL_COLLECTIONS \
      BTEST_WARN_EQUAL_COLLECTIONS
#   define WARN_PER_ELEMENT_EQ \
      BTEST_WARN_PER_ELEMENT_EQ
#   define WARN_PER_ELEMENT_EQUAL \
      BTEST_WARN_PER_ELEMENT_EQUAL
#   define WARN_ELEMENTWISE_EQ \
      BTEST_WARN_ELEMENTWISE_EQ
#   define WARN_ELEMENTWISE_EQUAL \
      BTEST_WARN_ELEMENTWISE_EQUAL
#   define WARN_BITWISE_EQ \
      BTEST_WARN_BITWISE_EQ
#   define WARN_BITWISE_EQUAL \
      BTEST_WARN_BITWISE_EQUAL
#   define WARN_PREDICATE \
      BTEST_WARN_PREDICATE
#   define WARN_MESSAGE \
      BTEST_WARN_MESSAGE
#   define WARN_NO_THROW \
      BTEST_WARN_NO_THROW
#   define WARN_THROW \
      BTEST_WARN_THROW
#   define WARN_EXCEPTION_PREDICATE \
      BTEST_WARN_EXCEPTION_PREDICATE
#   define WARN_EXCEPTION_BY_PREDICATE \
      BTEST_WARN_EXCEPTION_BY_PREDICATE
#   define WARN_EXCEPTION \
      BTEST_WARN_EXCEPTION
#   define WARN_EX_PREDICATE \
      BTEST_WARN_EX_PREDICATE
#   define WARN_EX_BY_PREDICATE \
      BTEST_WARN_EX_BY_PREDICATE
#   define WARN_EX \
      BTEST_WARN_EX
# endif
//
# if defined(USING_BTEST_AS_T) || defined(USING_BTEST_WARN_AS_T)
#   define T_WARN \
      BTEST_WARN
#   define T_WARN_IS \
      BTEST_WARN_IS
#   define T_WARN_TRUE \
      BTEST_WARN_TRUE
#   define T_WARN_T \
      BTEST_WARN_T
#   define T_WARN_FALSE \
      BTEST_WARN_FALSE
#   define T_WARN_F \
      BTEST_WARN_F
#   define T_WARN_GT \
      BTEST_WARN_GT
#   define T_WARN_GE \
      BTEST_WARN_GE
#   define T_WARN_LT \
      BTEST_WARN_LT
#   define T_WARN_LE \
      BTEST_WARN_LE
#   define T_WARN_NE \
      BTEST_WARN_NE
#   define T_WARN_NZ \
      BTEST_WARN_NZ
#   define T_WARN_EQ \
      BTEST_WARN_EQ
#   define T_WARN_EQUAL \
      BTEST_WARN_EQUAL
#   define T_WARN_ZERO \
      BTEST_WARN_ZERO
#   define T_WARN_Z \
      BTEST_WARN_Z
#   define T_WARN_CLOSE \
      BTEST_WARN_CLOSE
#   define T_WARN_CLOSE_FRACTION \
      BTEST_WARN_CLOSE_FRACTION
#   define T_WARN_SMALL \
      BTEST_WARN_SMALL
#   define T_WARN_EQ_COLLECTIONS \
      BTEST_WARN_EQ_COLLECTIONS
#   define T_WARN_EQUAL_COLLECTIONS \
      BTEST_WARN_EQUAL_COLLECTIONS
#   define T_WARN_PER_ELEMENT_EQ \
      BTEST_WARN_PER_ELEMENT_EQ
#   define T_WARN_PER_ELEMENT_EQUAL \
      BTEST_WARN_PER_ELEMENT_EQUAL
#   define T_WARN_ELEMENTWISE_EQ \
      BTEST_WARN_ELEMENTWISE_EQ
#   define T_WARN_ELEMENTWISE_EQUAL \
      BTEST_WARN_ELEMENTWISE_EQUAL
#   define T_WARN_BITWISE_EQ \
      BTEST_WARN_BITWISE_EQ
#   define T_WARN_BITWISE_EQUAL \
      BTEST_WARN_BITWISE_EQUAL
#   define T_WARN_PREDICATE \
      BTEST_WARN_PREDICATE
#   define T_WARN_MESSAGE \
      BTEST_WARN_MESSAGE
#   define T_WARN_NO_THROW \
      BTEST_WARN_NO_THROW
#   define T_WARN_THROW \
      BTEST_WARN_THROW
#   define T_WARN_EXCEPTION_PREDICATE \
      BTEST_WARN_EXCEPTION_PREDICATE
#   define T_WARN_EXCEPTION_BY_PREDICATE \
      BTEST_WARN_EXCEPTION_BY_PREDICATE
#   define T_WARN_EXCEPTION \
      BTEST_WARN_EXCEPTION
#   define T_WARN_EX_PREDICATE \
      BTEST_WARN_EX_PREDICATE
#   define T_WARN_EX_BY_PREDICATE \
      BTEST_WARN_EX_BY_PREDICATE
#   define T_WARN_EX \
      BTEST_WARN_EX
# endif
//
# if defined(USING_BTEST_AS_BT) || defined(USING_BTEST_WARN_AS_BT)
#   define BT_WARN \
      BTEST_WARN
#   define BT_WARN_IS \
      BTEST_WARN_IS
#   define BT_WARN_TRUE \
      BTEST_WARN_TRUE
#   define BT_WARN_T \
      BTEST_WARN_T
#   define BT_WARN_FALSE \
      BTEST_WARN_FALSE
#   define BT_WARN_F \
      BTEST_WARN_F
#   define BT_WARN_GT \
      BTEST_WARN_GT
#   define BT_WARN_GE \
      BTEST_WARN_GE
#   define BT_WARN_LT \
      BTEST_WARN_LT
#   define BT_WARN_LE \
      BTEST_WARN_LE
#   define BT_WARN_NE \
      BTEST_WARN_NE
#   define BT_WARN_NZ \
      BTEST_WARN_NZ
#   define BT_WARN_EQ \
      BTEST_WARN_EQ
#   define BT_WARN_EQUAL \
      BTEST_WARN_EQUAL
#   define BT_WARN_ZERO \
      BTEST_WARN_ZERO
#   define BT_WARN_Z \
      BTEST_WARN_Z
#   define BT_WARN_CLOSE \
      BTEST_WARN_CLOSE
#   define BT_WARN_CLOSE_FRACTION \
      BTEST_WARN_CLOSE_FRACTION
#   define BT_WARN_SMALL \
      BTEST_WARN_SMALL
#   define BT_WARN_EQ_COLLECTIONS \
      BTEST_WARN_EQ_COLLECTIONS
#   define BT_WARN_EQUAL_COLLECTIONS \
      BTEST_WARN_EQUAL_COLLECTIONS
#   define BT_WARN_PER_ELEMENT_EQ \
      BTEST_WARN_PER_ELEMENT_EQ
#   define BT_WARN_PER_ELEMENT_EQUAL \
      BTEST_WARN_PER_ELEMENT_EQUAL
#   define BT_WARN_ELEMENTWISE_EQ \
      BTEST_WARN_ELEMENTWISE_EQ
#   define BT_WARN_ELEMENTWISE_EQUAL \
      BTEST_WARN_ELEMENTWISE_EQUAL
#   define BT_WARN_BITWISE_EQ \
      BTEST_WARN_BITWISE_EQ
#   define BT_WARN_BITWISE_EQUAL \
      BTEST_WARN_BITWISE_EQUAL
#   define BT_WARN_PREDICATE \
      BTEST_WARN_PREDICATE
#   define BT_WARN_MESSAGE \
      BTEST_WARN_MESSAGE
#   define BT_WARN_NO_THROW \
      BTEST_WARN_NO_THROW
#   define BT_WARN_THROW \
      BTEST_WARN_THROW
#   define BT_WARN_EXCEPTION_PREDICATE \
      BTEST_WARN_EXCEPTION_PREDICATE
#   define BT_WARN_EXCEPTION_BY_PREDICATE \
      BTEST_WARN_EXCEPTION_BY_PREDICATE
#   define BT_WARN_EXCEPTION \
      BTEST_WARN_EXCEPTION
#   define BT_WARN_EX_PREDICATE \
      BTEST_WARN_EX_PREDICATE
#   define BT_WARN_EX_BY_PREDICATE \
      BTEST_WARN_EX_BY_PREDICATE
#   define BT_WARN_EX \
      BTEST_WARN_EX
# endif
