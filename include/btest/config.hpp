/// Preamble {{{
//  ==========================================================================
//        @file config.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-23 Monday 00:21:07 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_CONFIG_HPP_INCLUDED
# define BTEST_CONFIG_HPP_INCLUDED
//
# ifdef USING_BTEST
#   ifdef STATIC
#     ifdef BOOST_TEST_DYN_LINK
#       undef BOOST_TEST_DYN_LINK
#     endif
#   else
#     ifndef BOOST_TEST_DYN_LINK
#       define BOOST_TEST_DYN_LINK DYN_LINK
#     endif
#   endif
#   ifdef NO_LIB
#     ifndef BOOST_TEST_NO_LIB
#       define BOOST_TEST_NO_LIB   NO_LIB
#     endif
#   endif
#   ifdef NO_MAIN
#     ifndef BOOST_TEST_NO_MAIN
#       define BOOST_TEST_NO_MAIN  NO_MAIN
#     endif
#   endif
#   ifdef MAIN
#     ifndef BOOST_TEST_MAIN
#       define BOOST_TEST_MAIN     MAIN
#     endif
#   endif
#   ifdef MODULE
#     ifndef BOOST_TEST_MODULE
#       define BOOST_TEST_MODULE   MODULE
#     endif
#   endif
# endif
//
# ifdef USING_BTEST_AS_T
#   ifdef T_STATIC
#     ifdef BOOST_TEST_DYN_LINK
#       undef BOOST_TEST_DYN_LINK
#     endif
#   else
#     ifndef BOOST_TEST_DYN_LINK
#       define BOOST_TEST_DYN_LINK T_DYN_LINK
#     endif
#   endif
#   ifdef T_NO_LIB
#     ifndef BOOST_TEST_NO_LIB
#       define BOOST_TEST_NO_LIB   T_NO_LIB
#     endif
#   endif
#   ifdef T_NO_MAIN
#     ifndef BOOST_TEST_NO_MAIN
#       define BOOST_TEST_NO_MAIN  T_NO_MAIN
#     endif
#   endif
#   ifdef T_MAIN
#     ifndef BOOST_TEST_MAIN
#       define BOOST_TEST_MAIN     T_MAIN
#     endif
#   endif
#   ifdef T_MODULE
#     ifndef BOOST_TEST_MODULE
#       define BOOST_TEST_MODULE   T_MODULE
#     endif
#   endif
# endif
//
# ifdef USING_BTEST_AS_BT
#   ifdef BT_STATIC
#     ifdef BOOST_TEST_DYN_LINK
#       undef BOOST_TEST_DYN_LINK
#     endif
#   else
#     ifndef BOOST_TEST_DYN_LINK
#       define BOOST_TEST_DYN_LINK BT_DYN_LINK
#     endif
#   endif
#   ifdef BT_NO_LIB
#     ifndef BOOST_TEST_NO_LIB
#       define BOOST_TEST_NO_LIB   BT_NO_LIB
#     endif
#   endif
#   ifdef BT_NO_MAIN
#     ifndef BOOST_TEST_NO_MAIN
#       define BOOST_TEST_NO_MAIN  BT_NO_MAIN
#     endif
#   endif
#   ifdef BT_MAIN
#     ifndef BOOST_TEST_MAIN
#       define BOOST_TEST_MAIN     BT_MAIN
#     endif
#   endif
#   ifdef BT_MODULE
#     ifndef BOOST_TEST_MODULE
#       define BOOST_TEST_MODULE   BT_MODULE
#     endif
#   endif
# endif
//
# ifdef BTEST_STATIC
#   ifdef BOOST_TEST_DYN_LINK
#     undef BOOST_TEST_DYN_LINK
#   endif
# else
#   ifndef BOOST_TEST_DYN_LINK
#     define BOOST_TEST_DYN_LINK BTEST_DYN_LINK
#   endif
# endif
# ifdef BTEST_NO_LIB
#   ifndef BOOST_TEST_NO_LIB
#     define BOOST_TEST_NO_LIB   BTEST_NO_LIB
#   endif
# endif
# ifdef BTEST_NO_MAIN
#   ifndef BOOST_TEST_NO_MAIN
#     define BOOST_TEST_NO_MAIN  BTEST_NO_MAIN
#   endif
# endif
# ifdef BTEST_MAIN
#   ifndef BOOST_TEST_MAIN
#     define BOOST_TEST_MAIN     BTEST_MAIN
#   endif
# endif
# ifdef BTEST_MODULE
#   ifndef BOOST_TEST_MODULE
#     define BOOST_TEST_MODULE   BTEST_MODULE
#   endif
# endif
//
# include <boost/test/detail/config.hpp>
//
# ifndef BOOST_TEST_NO_OLD_TOOLS
#   define BOOST_TEST_NO_OLD_TOOLS
# endif
//
# endif // BTEST_CONFIG_HPP_INCLUDED
