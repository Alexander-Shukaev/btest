/// Preamble {{{
//  ==========================================================================
//        @file exception.hpp
//  --------------------------------------------------------------------------
//     @version 0.0.0
//  --------------------------------------------------------------------------
//     @updated 2017-01-15 Sunday 17:49:46 (+0100)
//  --------------------------------------------------------------------------
//     @created 2016-04-03 Sunday 00:18:25 (+0200)
//  --------------------------------------------------------------------------
//      @author Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//  @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
//  --------------------------------------------------------------------------
//   @copyright Copyright (C) 2017,
//              Alexander Shukaev <http://Alexander.Shukaev.name>.
//              All rights reserved.
//  --------------------------------------------------------------------------
//     @license This program is free software: you can redistribute it and/or
//              modify it under the terms of the GNU General Public License as
//              published by the Free Software Foundation, either version 3 of
//              the License, or (at your option) any later version.
//
//              This program is distributed in the hope that it will be
//              useful, but WITHOUT ANY WARRANTY; without even the implied
//              warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//              PURPOSE.  See the GNU General Public License for more details.
//
//              You should have received a copy of the GNU General Public
//              License along with this program.  If not, see
//              <http://www.gnu.org/licenses/>.
//  ==========================================================================
//  }}} Preamble
//
// clang-format off
# ifndef BTEST_EXCEPTION_HPP_INCLUDED
# define BTEST_EXCEPTION_HPP_INCLUDED
//
# include "config"
# include "diagnostic"
# include "void_cast"
//
# include <boost/preprocessor/cat.hpp>
//
# ifdef NDEBUG
#   ifndef BTEST_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#     define BTEST_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   endif
# endif
//
# ifdef BTEST_EXCEPTION_LOG_DIAGNOSTIC_DISABLED
#   define BTEST_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_VOID_CAST(e)
# else
#   define BTEST_EXCEPTION_LOG_DIAGNOSTIC(E) \
      BTEST_LOG_DIAGNOSTIC(e)
# endif
//
# ifndef BTEST_EXCEPTION_PREDICATE_IDENTIFIER_SUFFIX
#   define BTEST_EXCEPTION_PREDICATE_IDENTIFIER_SUFFIX \
      _EXCEPTION_PREDICATE
# endif
//
# define BTEST_EXCEPTION_PREDICATE_IDENTIFIER_BY_LEVEL(L) \
    BOOST_PP_CAT(L, BTEST_EXCEPTION_PREDICATE_IDENTIFIER_SUFFIX)
//
# define BTEST_EXCEPTION_PREDICATE_BY_LEVEL(L, E) \
    bool \
    BTEST_##L##_EXCEPTION_PREDICATE_IDENTIFIER(E const& e)
//
# define BTEST_EXCEPTION_BY_PREDICATE_BY_LEVEL(L, S, E, P) \
    do { \
      struct BTEST { \
        static BTEST_##L##_EXCEPTION_PREDICATE(E) { \
          BTEST_##L##_EXCEPTION_LOG_DIAGNOSTIC(e); \
          return P(e); \
        } \
      }; \
      \
      BOOST_##L##_EXCEPTION( \
          S, E, BTEST::BTEST_##L##_EXCEPTION_PREDICATE_IDENTIFIER); \
    } while(false)
//
# define BTEST_EXCEPTION_BY_LEVEL(L, S, E, I) \
    BTEST_##L##_EXCEPTION_BY_PREDICATE( \
        S, E, I::BTEST_##L##_EXCEPTION_PREDICATE_IDENTIFIER)
//
# endif // BTEST_EXCEPTION_HPP_INCLUDED
